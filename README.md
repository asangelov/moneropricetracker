# Monero Price in EUR

## Local Installation

Install Parcel if you don't already have it:

`npm install -g parcel-bundler`

Clone the repository:

`git clone https://gitlab.com/asangelov/moneropricetracker.git`

Change directory:

`cd moneropricetracker/src/`

Run the app:

`parcel index.html`

The app should now be running on http://localhost:1234 if the port was free.

